# Read from a CSV file, multiple two columns, and then write back to the CSV file.

class CsvFile

	require 'csv'

	class << self
		def csv_update
			append_array = csv_read
			csv_write(append_array)
		end
			
		def csv_read
			append_array = []
			if File.empty?('./res/input.csv')
				raise "File is empty"
			end
			CSV.foreach('./res/input.csv', converters: :numeric, headers: true, skip_blanks: true) do |row| 
				if row['c1'] == nil || row['c2'] == nil
					raise "Column value does not exist"
				end
				csv_row = [row['c1'],row['c2'],row['c1']*row['c2']]
				append_array.push(csv_row)
			end
			append_array
		rescue TypeError
			puts "oops: characters appeared in place of number"
		rescue Errno::ENOENT 
			puts "oops: CSV file not found"
		rescue Errno::EACCES
			puts "oops: file can't be accessed"
		rescue RuntimeError => error	
			puts "oops: #{error}"
		end

		def csv_write(append_array)
			CSV.open('./res/output.csv', 'w') do |csv_object|
				append_array.each do |row_append|
					csv_object << row_append
				end
			end
		rescue NoMethodError
			puts "Cant write to file"
		end
	end
end

CsvFile.csv_update


=begin
Content of Original Csv
[["5", "4"], ["15", "20"], ["2", "5"]]
Content of csv after performing operation
[["5", "4", "20"], ["15", "20", "300"], ["2", "5", "10"]] 
=end