# Use the map function to double all the elements in the array. 

class MapArray

  def self.double_element_exception(single_array) 
    single_array.split(' ').map { |char| char =~ /[a-zA-Z]/ ? next : char.to_i * 2 }.compact
  end
end

puts "Example working of program"
puts alpha_numeric = "1 2 3 4 5 a 6 7 b  8"
print MapArray.double_element_exception(alpha_numeric),"\n"

puts "Enter the alpanumeric values to be counted"
alpha_numeric = gets
print MapArray.double_element_exception(alpha_numeric),"\n"

=begin
Static Input
Example working of program
Input 1 2 3 4 5 a 6 7 b  8
Output [2, 4, 6, 8, 10, 12, 14, 16]

Dynamic Input
Enter the alpanumeric values to be counted
Input 1 2 a b 3 4 c d 5 6
Output [2, 4, 6, 8, 10, 12] 		
=end