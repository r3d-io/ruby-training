p "at topmost level self is #{self}"

class MyClass
  p "in class definition block self is #{self}"

  def self.my_class_method
    p "in my_class_method self is #{self}"
  end  

  def my_instance_method
    p "in my_instance_method self is #{self}"
  end  
end  

MyClass.my_class_method
MyClass.new.my_instance_method
=begin
class Person
   def initialize(first, last, age)
        @first_name = first
        @last_name = last
        @age = age
   end

   def birthday
        @age += 1
   end

   def introduce
         puts "Hi everyone. My name is #{@first_name} #{@last_name}."
   end
end

class Student < Person
   def initialize(first, last, age,height)
      super(first, last, age)
      @height = height
   end

   def introduce
         puts "Hello teacher. My name is #{@first_name} #{@last_name} #{@age} #{@height}."
   end
end

class Teacher < Person
   def introduce
         puts "Hello class. My name is #{@first_name} #{@last_name}."
   end

end

class Parent < Person
   def introduce
       puts "Hi. I'm one of the parents. My name is #{@first_name} #{@last_name}."
   end
end

john = Student.new("John", "Doe", 18,5)
john.introduce   #=> Hello teacher. My name is John Doe.
=end

=begin
class Parent
   private
   def name
     'Mommy'
   end
 end
 
 class Child < Parent
   def get_parent_name
     # Implicit receiver
     puts name
 
     # Explicit receiver
     puts self.name # rescue puts 'NoMethodError'
 
     # Explicit receiver
     puts Parent.new.name # rescue puts 'NoMethodError'
   end
 end

 Child.new.get_parent_name
=end

=begin
5.times { print "We *love* Ruby -- it's outrageous!" }
=end

=begin
CONST = ' out there'
class Inside_one
   CONST = proc {' in there'}
   def where_is_my_CONST
      ::CONST + ' inside one'
   end
end
class Inside_two
   CONST = ' inside two'
   def where_is_my_CONST
      CONST
   end
end
puts Inside_one.new.where_is_my_CONST
puts Inside_two.new.where_is_my_CONST
puts Object::CONST + Inside_two::CONST
puts Inside_two::CONST + CONST
puts Inside_one::CONST
puts Inside_one::CONST.call + Inside_two::CONST
=end
